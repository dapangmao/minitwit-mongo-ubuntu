minitwit-mongodb
====

What is minitwit-mongodb?

A MongoDB powered fork of Minitwit. (https://github.com/mitsuhiko/flask/tree/master/examples/minitwit)

Prerequisites
====

MongoDB installed on your machine, running and listening on port 27017.

Installation
====

1. Download minitwitr-mongodb, then:

        cd minitwit-mongodb/

2. Create a virtualenv and activate it:

        virtualenv env
        source env/bin/activate

3. Install the required packages:

        pip install -r requirements.txt

4. Run the application:

        python minitwit.py

5. Open your favorite browser and go to http://127.0.0.1:5000/
